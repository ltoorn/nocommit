// MIT License
//
// Copyright © 2019 Lucas Christiaan van den Toorn
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the Software), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// main of nocommit implements a simple pre-commit program usable as pre-commit hook
// in git, by adding it to the .git/hooks directory with the name 'pre-commit'.
//
// nocommit scans the source files in the code base for the string 'nocommit' and exits
// with an exit code of 1 if it finds any.
package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

var allowedExtensions = []string{
	"adb",    // ada
	"adoc",   // asciidoc
	"ads",    // ada
	"asm",    // assembly
	"c",      // c
	"clj",    // clojure
	"cpp",    // c++
	"cs",     // c sharp
	"css",    // css
	"csx",    // c sharp
	"c++",    // c++
	"d",      // d
	"hs",     // haskell
	"go",     // go
	"groovy", // groovy
	"gsh",    // groovy
	"gvy",    // groovy
	"gy",     // groovy
	"html",   // html
	"java",   // java
	"jl",     // julia
	"js",     // javascript
	"jsp",    // jsp
	"kt",     // kotlin
	"kts",    // kotlin
	"less",   // less
	"lisp",   // common lisp
	"lua",    // lua
	"md",     // markdown
	"ml",     // ocaml
	"mli",    // ocaml
	"pl",     // perl
	"pm",     // perl
	"plx",    // perl
	"pod",    // perl
	"py",     // python
	"rb",     // ruby
	"rs",     // rust
	"scm",    // scheme
	"sc",     // scala
	"scala",  // scala
	"sml",    // standard ml
	"swift",  // swift
	"t",      // perl
	"txt",    // plain text
	"wsdl",   // wsdl
	"xml",    // xml
	"xs",     // perl
	"xsd",    // xsd
}

var (
	encounteredExtensions []string
	fileContentBuffer     []byte
	fileNamesToPrint      []string
)

var target = []byte("nocommit")

func main() {
	exitCode := 0

	err := filepath.Walk(".", nocommitWalker)
	if err != nil {
		exitCode = 1
		fmt.Println(err)
	}

	if len(fileNamesToPrint) > 0 {
		exitCode = 1
		fmt.Print("Encountered a nocommit in the following file(s):")
		for _, fileName := range fileNamesToPrint {
			fmt.Printf("\n\t- %q", fileName)
		}
		fmt.Printf("\n")
	}

	os.Exit(exitCode)
}

func nocommitWalker(path string, info os.FileInfo, err error) error {

	closeFile := func(f *os.File) {
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}

	if err != nil {
		fmt.Println(err)
		return nil
	}

	if info.IsDir() {
		if info.Name() == ".git" {
			// We always want to skip the .git folder, since that will contain the executable
			// of this program, which contains the bytes of the string literal 'nocommit' somewhere in some
			// data section.
			return filepath.SkipDir
		}
		return nil
	}

	if !isExtensionWhitelisted(info.Name()) {
		return nil
	}

	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer closeFile(file)

	ensureSizeFileContentBuffer(info.Size())

	_, err = file.Read(fileContentBuffer)
	if err != nil && err != io.EOF {
		return err
	}

	if hasTarget(fileContentBuffer) {
		fileNamesToPrint = append(fileNamesToPrint, info.Name())
	}

	return nil
}

// ensureSizeFileContentBuffer ensures that the fileContentBuffer is large enough to hold size bytes.
// If it isn't we either double the capacity or set the capacity to size if size is larger than double
// the current capacity of the buffer.
func ensureSizeFileContentBuffer(size int64) {
	currentCap := int64(cap(fileContentBuffer))
	if currentCap < size {
		newCap := 2 * currentCap
		if newCap < size {
			newCap = size
		}
		fileContentBuffer = make([]byte, size, newCap)
		return
	}
	fileContentBuffer = fileContentBuffer[:size]
}

// hasTarget tries to find target in the fileContent byte slice. Because target is hardcoded,
// we can bake in some assumptions to avoid re-examining characters as much as possible.
func hasTarget(fileContent []byte) bool {
	targetIndex := 0
	lengthTarget := len(target)
	lengthFileContent := len(fileContent)
	for i := 0; i < lengthFileContent; i++ {
		if fileContent[i] == target[targetIndex] {
			targetIndex++
			if targetIndex == lengthTarget {
				return true
			}
		} else {
			targetIndex = 0
			if fileContent[i] == 'n' {
				targetIndex++
			}
		}
	}
	return false
}

// isExtensionWhitelisted determines whether fileName represents a file in which we need to
// search for target. If fileName belongs to an allowed file, we add it to encounteredExtensions
// to avoid having to iterate through the allowedExtensions repeatedly, since the common case for a code base
// will be to encounter the same few extensions over and over again.
func isExtensionWhitelisted(fileName string) bool {
	subs := strings.Split(fileName, ".")
	ext := subs[len(subs)-1]
	for _, encountered := range encounteredExtensions {
		if ext == encountered {
			return true
		}
	}
	for _, whitelisted := range allowedExtensions {
		if ext == whitelisted {
			encounteredExtensions = append(encounteredExtensions, ext)
			return true
		}
	}
	return false
}
